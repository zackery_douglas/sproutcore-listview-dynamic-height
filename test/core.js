Test = SC.Application.create({NAMESPACE: 'Test', VERSION: '0.1.0', store: SC.Store.create().from(SC.Record.fixtures) });

Test.Top = SC.Record.extend({
  middles: SC.Record.toMany(
    SC.Record,
    {
      isMaster: NO,
      isNested: YES
    }
  )
});

Test.TopC = SC.ObjectController.create();

Test.MiddlesC = SC.ArrayController.create({
  isEditable: YES,
  contentBinding: 'Test.TopC*content.middles',
  _: function () {
    console.debug(this, arguments);
  }.observes('content')
});

Test.TopC.set('content', Test.store.createRecord(Test.Top, {}));