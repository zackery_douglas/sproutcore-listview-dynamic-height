Test.mainPage = SC.Page.design({
  mainPane: SC.MainPane.design({
    childViews: 'mainView'.w(),
    
    mainView: SC.View.design({
      layout: { top: 0, height: 900 },
      childViews: 'add tooSmall exact tooLarge'.w(),
      add: SC.ButtonView.design({
          layout: { top: 0, height: 28 },
          title: '+',
          action: function () {
              Test.MiddlesC.addObject({ fred:'wilma' });
          }
      }),
      tooSmall: SC.ScrollView.design({
          layout: { top: 30, minHeight: 40 },
          contentView: SC.ListView.design({
            layout: { height: 10 },
            rowHeight: 20,
            contentBinding: 'Test.MiddlesC.arrangedObjects',
            contentObs: function () {
              console.debug(this, arguments);
            }.observes('content'),
            exampleView: SC.LabelView.extend({
              contentValueKey: 'fred',
              _: function () {
                console.debug(this.get('content'));
              }.observes('content', 'value')
            })
        })
      }),
      exact: SC.ScrollView.design({
          layout: { top: 70, minHeight: 40 },
          contentView: SC.ListView.design({
            layout: { height: 20 },
            rowHeight: 20,
            contentBinding: 'Test.MiddlesC.arrangedObjects',
            contentObs: function () {
              console.debug(this, arguments);
            }.observes('content'),
            exampleView: SC.LabelView.extend({
              contentValueKey: 'fred',
              _: function () {
                console.debug(this.get('content'));
              }.observes('content', 'value')
            })
        })
      }),
      tooLarge: SC.ScrollView.design({
          layout: { top: 110, minHeight: 40 },
          contentView: SC.ListView.design({
            layout: { height: 21 },
            rowHeight: 20,
            contentBinding: 'Test.MiddlesC.arrangedObjects',
            contentObs: function () {
              console.debug(this, arguments);
            }.observes('content'),
            exampleView: SC.LabelView.extend({
              contentValueKey: 'fred',
              _: function () {
                console.debug(this.get('content'));
              }.observes('content', 'value')
            })
        })
      })
    })
  })
});
